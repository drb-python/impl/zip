.. _install:

Installation of zip driver
====================================
Installing ``drb-driver-zip`` with execute the following in a terminal:

.. code-block::

    pip install drb-driver-zip
