===================
Data Request Broker
===================
---------------------------------
ZIP driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-zip/month
    :target: https://pepy.tech/project/drb-driver-zip
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-zip.svg
    :target: https://pypi.org/project/drb-driver-zip/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-zip.svg
    :target: https://pypi.org/project/drb-driver-zip/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-zip.svg
    :target: https://pypi.org/project/drb-driver-zip/
    :alt: Python Version Support Badge

-------------------

This drb-driver-zip module implements access to zip containers with DRB data model.
It is able to navigates among the zip contents.


User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

