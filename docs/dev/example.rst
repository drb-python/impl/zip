.. _example:

Examples
=========

Open and read a zip container
-----------------------------
.. literalinclude:: example/open.py
    :language: python
