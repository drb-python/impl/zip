from drb.exceptions.core import DrbException


class DrbZipNodeException(DrbException):
    pass
